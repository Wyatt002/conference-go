import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Presentation
from events.models import Conference


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentation = Presentation.objects.filter(conference_id=conference_id)
        return JsonResponse(
            {"presentation": presentation},
            encoder=PresentationListEncoder,
        )
    else:
        content = json.loads(request.body)

    # Get the Conference object and put it in the content dict
    try:
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )

    presentation = Presentation.create(**content)
    return JsonResponse(
        presentation,
        encoder=PresentationListEncoder,
        safe=False,
    )


# def api_list_presentations(request, conference_id):
#     """
#     Lists the presentation titles and the link to the
#     presentation for the specified conference id.

#     Returns a dictionary with a single key "presentations"
#     which is a list of presentation titles and URLS. Each
#     entry in the list is a dictionary that contains the
#     title of the presentation, the name of its status, and
#     the link to the presentation's information.

#     {
#         "presentations": [
#             {
#                 "title": presentation's title,
#                 "status": presentation's status name
#                 "href": URL to the presentation,
#             },
#             ...
#         ]
#     }
#     """
#     presentations = [
#         {
#             "title": p.title,
#             "status": p.status.name,
#             "href": p.get_api_url(),
#         }
#         for p in Presentation.objects.filter(conference=conference_id)
#     ]
#     return JsonResponse({"presentations": presentations})


# presentation = Presentation.objects.get(id=id)
# return JsonResponse({
#     "presenter_name": presentation.presenter_name,
#     "company_name": presentation.company_name,
#     "presenter_email": presentation.presenter_email,
#     "title": presentation.title,
#     "synopsis": presentation.synopsis,
#     "created": presentation.created,
#     "status": presentation.status.name,
#     "conference": {
#         "name": presentation.conference.name,
#         "href": presentation.conference.get_api_url(),
#     }

# })


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"delete": count > 0})
    else:
        content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
